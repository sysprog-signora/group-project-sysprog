#include <linux/init.h> 
#include <linux/module.h> 
#include <linux/device.h> 
#include <linux/kernel.h>
#include <linux/fs.h>   
#include <asm/uaccess.h> 
#include <linux/cdev.h>

#define  DEVICE_NAME "sgdrv"    // /dev/<value>
#define  CLASS_NAME  "sg"          // The device class

MODULE_LICENSE("GPL");            
MODULE_AUTHOR("Signora");    
MODULE_DESCRIPTION("Le simple Linux char driver");
MODULE_VERSION("0.1");        
static int    majorNumber;                  
static char   message[256] = {0};           
static short  size_of_message;         
static int    numberOpens = 0;              
static struct class*  classptr  = NULL;     
static struct device* dvcptr = NULL;  
static struct cdev* kernel_cdev;
static dev_t devno;      

// The prototype functions for the character driver
static int     dev_open(struct inode *, struct file *);
static int     dev_release(struct inode *, struct file *);
static ssize_t dev_read(struct file *, char *, size_t, loff_t *);
static ssize_t dev_write(struct file *, const char *, size_t, loff_t *);

static struct file_operations fops =
{
   .open = dev_open,
   .read = dev_read,
   .write = dev_write,
   .release = dev_release,
};


static int dev_open(struct inode *inodep, struct file *filep){
   numberOpens++;
   printk(KERN_INFO "RDcdevdr: Device has been opened %d time(s)\n", numberOpens);
   return 0;
}

static int dev_release(struct inode *inodep, struct file *filep){
   printk(KERN_INFO "RDcdevdr: Device successfully closed\n");
   return 0;
}


static int __init rdchardrv_init(void){
   int status = alloc_chrdev_region(&devno, 0, 1, DEVICE_NAME);
   if (status < 0) {
    printk(KERN_INFO "sgdrv: Major number allocation failed\n");
    return status;
   }
   majorNumber = MAJOR(devno);
   printk(KERN_INFO "sgdrv: Major number: %d\n", majorNumber);
   if (majorNumber<0) return majorNumber;

   // add cdev
   kernel_cdev = cdev_alloc();
   kernel_cdev->ops = &fops;
   kernel_cdev->owner = THIS_MODULE;
   status = cdev_add(kernel_cdev, devno, 1);
   if (status < 0) {
   	printk(KERN_INFO "sgdrv: Unable to add cdev");
   	return status;
   }
   
   // Register the device class
   classptr = class_create(THIS_MODULE, CLASS_NAME);
   if (IS_ERR(classptr)){
           unregister_chrdev(majorNumber, DEVICE_NAME);
           return PTR_ERR(classptr);
   }


   // Register the device driver
   dvcptr = device_create(classptr, NULL, MKDEV(majorNumber, 0), NULL, DEVICE_NAME);
   if (IS_ERR(dvcptr)){
          class_destroy(classptr);
          unregister_chrdev(majorNumber, DEVICE_NAME);
          return PTR_ERR(dvcptr);
   }
   printk(KERN_INFO "sgdrv: device class created correctly\n");
   return 0;
}


static void __exit rdchardrv_exit(void){
    cdev_del(kernel_cdev);
    device_destroy(classptr, MKDEV(majorNumber, 0));      
    class_unregister(classptr);                          
    class_destroy(classptr);                              
    unregister_chrdev(majorNumber, DEVICE_NAME);              
    printk(KERN_INFO "sgdrv: Goodbye from the LKM!\n");
}

// standard prime checker. Loop dari 2 sampai sqrt(N).
bool isPrime(int num) {

	// special cases
	if(num<2) {
		return false;
	}

	int i;
	for(i=2;i*i<=num;i++) {
		if(num%i==0) return false;
	}
	return true;
}

static ssize_t dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset){
    // precaution jika data kosong
    size_t bytesToCopy = len >= size_of_message ? size_of_message: len;
    size_t bytesNotCopied = 0;
    if(!bytesToCopy) return 0;

    int num = 0, msg_len = strlen(message);

    // membuat angka dari depan, dan sekaligus checking apakah ada angka aneh
    bool isValidInteger=true;

    if(msg_len > 9) {
    	sprintf(message, "Number is too big, must be less than 1.000.000.000");
    	isValidInteger = false;
    }
    int i;
    for(i=0; i < msg_len; i++) {
    	if(message[i] < '0' || message[i] > '9') {
    		isValidInteger = false;
    		break;
    	}

    	num = num*10 + (message[i] - '0');
    }

    if(isValidInteger) {
    	if(isPrime(num)) {
    		sprintf(message, "%d is a prime number", num);
    	} else {
    		sprintf(message, "%d is not a prime number", num);
    	}
    } else {
    	sprintf(message, "%s is an invalid number", message);
    }

bytesNotCopied = raw_copy_to_user(buffer, message, sizeof(message));
    if(bytesNotCopied){
         return -EFAULT;
    }
    size_of_message = 0;
    return bytesToCopy;
}


static ssize_t dev_write(struct file *filep,
const char *buffer, size_t len, loff_t *offset){

    const size_t maxLen = 256 - 1;
    size_t bytesToCopy = len >= maxLen ? maxLen: len;
    size_t bytesNotCopied = 0;

    memset(message, '\0', sizeof(message));

    bytesNotCopied = raw_copy_from_user(message, buffer,bytesToCopy);
    size_of_message = bytesToCopy - bytesNotCopied;

    if(bytesNotCopied)
            return -EFAULT;
    return bytesToCopy;
}

module_init(rdchardrv_init);
module_exit(rdchardrv_exit);
