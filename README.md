Simple volume control application using Alsa.

# Dependency

pyalsaaudio
```
pip3 install pyalsaaudio
```

tkinter
```
sudo apt-get install python3-tk
```


# Running

```
python3 volume-control.py
```

# Bootscript

To run application in start-time, use bootscript python3.desktop
